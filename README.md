# Low level Node.js debug with Docker

* What is low-level debug
* Use cases of low-level debug
* Core dumps within docker container
* Inspection of Node.js core dumps with LLVM/llnode: re-creating stack, inspecting memory

See presentation: node_inspect.odp (WIP) in this repo.
