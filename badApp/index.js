const express = require('express')
const bodyParser = require('body-parser')
const package = require('./package');
const _ = require('lodash');

const app = express()

app.use(bodyParser.json({limit: '100mb', extended: true}))

app.get('/version', (req, res) => {
    res.json({ version: package.version });
});

app.get('/healthz', (req, res) => {
    res.json({ ok: true })
});

app.get('/readyz', (req, res) => {
    res.json({ ready: true })
});

app.post('/uniq', (req, res) => {
    res.json(_.uniqWith(req.body.what, _.isEqual));
})

app.listen(8765, ()=> {
    console.log('Server ready on 8765');
});
