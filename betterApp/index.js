const express = require('express')
const bodyParser = require('body-parser')
const package = require('./package');
const _ = require('lodash');

const app = express()

app.use(bodyParser.json({limit: '100mb', extended: true}))

app.get('/version', (req, res) => {
    res.json({ version: package.version });
});

app.get('/healthz', (req, res) => {
    res.json({ ok: true })
});

app.get('/readyz', (req, res) => {
    res.json({ ready: true })
});

app.post('/uniq', (req, res) => {
    const uniq = new Set();
    req.body.what.forEach(m => uniq.add(JSON.stringify(m)));
    res.json(Array.from(uniq).map(k => JSON.parse(k)));

})

app.listen(8765, ()=> {
    console.log('Server ready on 8765');
});
