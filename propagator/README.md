# Propagator - debug concept

Allows external debugging of multiple node.js processes which spawn each other 

* Replaces node.js within docker container with wrapper, which spawns node with --inspect-brk at free port between 7000 and 8000
* Useful if node is being spawned a lot as a subprocess - debugger can be attached to any of them
* It will write opened ports to /ports within container

## Steps for current example

* ./build.sh
* ./run_normal.sh
* See port in console or /ports in container
* Attach debugger and press continue
* See port of another spawned process in /ports
* Attach to it
